const { validationResult } = require('express-validator');

const Task = require('../models/Task');

//get all tasks
module.exports.getAllTasks = async (req, res) => {
  try {
    const tasks = await Task.find({ user: req.user.id }).sort({
      completeDate: -1
    });
    res.status(200).json(tasks);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

// create a new task
module.exports.createNewTask = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { name, body, completeDate, priority } = req.body;

  const newTask = {
    user: req.user.id,
    name,
    body,
    completeDate,
    priority
  };

  try {
    const task = await Task.create(newTask);
    res.status(201).json(task);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

// change data of task
module.exports.updateTask = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const taskId = req.params.id;
  const userId = req.user.id;
  const taskData = {};
  const parameters = req.body;

  Object.keys(parameters).forEach(param => {
    if (param === 'body' || param === 'name') {
      if (!parameters[param].trim()) return;
    }
    taskData[param] = parameters[param];
  });

  try {
    const task = await Task.findOneAndUpdate(
      { _id: taskId, user: userId },
      { $set: taskData },
      { new: true }
    );
    if (!task) {
      return res.status(404).json({ msg: 'No tasks found' });
    }
    return res.status(200).json(task);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

// //delete task by ID
module.exports.deleteTask = async (req, res) => {
  const taskId = req.params.id;
  const userId = req.user.id;

  try {
    const task = await Task.findOneAndDelete({
      _id: taskId,
      user: userId
    });
    if (!task) return res.status(404).json({ msg: 'Task not found' });
    res.status(200).json({ msg: 'Task was successfully deleted' });
  } catch (error) {
    res.status(500).json({ msg: 'Delete failed. Invalid ID' });
  }
};

// delete all completed tasks and return all not completed
module.exports.deleteCompletedTasks = async (req, res) => {
  const userId = req.user.id;

  try {
    const { n } = await Task.deleteMany({ user: userId, completed: true });
    if (n > 0) {
      try {
        const tasks = await Task.find({ user: req.user.id }).sort({
          completeDate: -1
        });
        return res.status(200).json({ deleted: n, tasks: tasks });
      } catch (error) {
        return res.status(500).json({ msg: error.message });
      }
    }
    res.status(400).json({ msg: 'No items was deleted' });
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

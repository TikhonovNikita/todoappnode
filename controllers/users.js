const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { validationResult } = require('express-validator');

const User = require('../models/User');
const Task = require('../models/Task');

module.exports.createNewUser = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }

  try {
    const { name, email, password } = req.body;
    let user = await User.findOne({ email });

    if (user) {
      return res.status(400).json({ errors: [{ msg: 'User already exists' }] });
    }

    user = new User({
      name,
      email,
      password
    });

    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);

    await user.save();

    const payload = {
      user: {
        id: user.id
      }
    };
    jwt.sign(
      payload,
      config.get('jwtSecret'),
      { expiresIn: 3600 },
      (error, token) => {
        if (error) {
          throw error;
        }
        res.json(token);
      }
    );
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ msg: error.message });
  }
};

//delete user and all related tasks
module.exports.deleteUser = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const userId = req.user.id;

  try {
    const { n } = await Task.deleteMany({ user: userId });
    try {
      const user = await User.findByIdAndDelete(userId);
      if (!user) return res.status(404).json({ msg: 'User not found' });
      res
        .status(200)
        .json({ msg: 'User was successfully deleted', deletedTasks: n });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

const { check, checkSchema } = require('express-validator');

const auth = require('../middleware/auth');

module.exports = [
  auth,
  checkSchema({
    priority: {
      in: 'body',
      matches: {
        options: [/^$|low|medium|high/],
        errorMessage: 'Invalid priority type'
      }
    }
  }),
];

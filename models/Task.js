const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  body: {
    type: String,
    required: true,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completeDate: {
    type: Date,
    default: Date.now
  },
  priority: {
    type: String,
    enum: ['', 'low', 'medium', 'high'],
    default: ''
  }
});

module.exports = Task = mongoose.model('task', TaskSchema);

To run app go to root folder:
`cd testTodoNode`
Then run
`npm run server`
It have a working API with registration, JWT auth and CRUD functionality for Tasks

**To Create User:**
send _PUT_ request to `api/users` with following fields
name, email, password, passwordConfirmation
for example

```
{
	"name": "Nikita Tikhonov",
	 "email": "war@gmdail.cm",
	 "password": "AAqwe1!ds",
	 "passwordConfirmation": "AAqwe1!ds"
}
```

we will get token.
Toket should be pasted into headers with key _x-auth-token_
for example:

````
x-auth-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNWQyY2MxYmY1YzczY2Q3YmJhOWY4MTZjIn0sImlhdCI6MTU2MzIxNDI3MSwiZXhwIjoxNTYzMjE3ODcxfQ.lgzHtqtXRWGryTSMJloZC_jxvFdoDQrJW4JicNwaLis```
**To authenticate existing user**
send POST request to ` api/auth` with following fields:
*email, password*
for example:
````

{
"email": "war@gmdail.cm",
"password": "AAqwe1!ds"
}

```
we will get token.

Also we can delete user. All of related tasks will be deleted too.

Then there are several routes for CRUD operations with Tasks.
We can create a new task, update task, delete all completed tasks, delete task by ID.
All of this  routes are private. So we need to pass * x-auth-token.*
Example route for creating task:
PUT to  `api/tasks`
in body should be valid task data,for example:
```

{
"name": "Test",
"body": "Test body",
"completeDate": "2019-09-15"
}

```
Also there are some validations to play with.

Thanks for testing my app!
```

const express = require('express');
const router = express.Router();
const { check, checkSchema } = require('express-validator');

const auth = require('../../middleware/auth');
const tasksController = require('../../controllers/tasks');
const validator = require('../../utils/validator');

// @route  GET api/tasks
// @desc   Get all tasks for current user
// @access Private
router.get('/', auth, tasksController.getAllTasks);

// @route  PUT api/tasks
// @desc   Create task for current user
// @access Private
router.put(
  '/',
  [
    ...validator,
    check('body', 'Body can not be empty')
      .trim()
      .isLength({ min: 1 }),
    check('name', 'Name can not be empty')
      .trim()
      .isLength({ min: 1 })
  ],
  tasksController.createNewTask
);

// @route  PATCH api/tasks
// @desc   Update task by it's id
// @access Private
router.patch('/:id', validator, tasksController.updateTask);

// @route  DELETE api/tasks
// @desc   Delete task by it's id
// @access Private
router.delete('/:id', auth, tasksController.deleteTask);

// @route  POST api/tasks
// @desc   Delete all completed tasks for current user
// @access Private
router.post('/', auth, tasksController.deleteCompletedTasks);

module.exports = router;

const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

const authController = require('../../controllers/auth');

// @route  POST api/auth
// @desc   Authentication route
// @access Public
router.post(
  '/',
  [
    check('email', 'please, include a valid email').isEmail(),
    check('password', 'password is required').exists()
  ],
  authController.authenticate
);

module.exports = router;

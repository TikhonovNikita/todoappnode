const express = require('express');
const router = express.Router();
const { check, body } = require('express-validator');

const auth = require('../../middleware/auth');
const usersController = require('../../controllers/users');


// @route  PUT api/users
// @desc   Register route
// @access Public
router.put(
  '/',
  [
    check('name', 'Name is required')
      .not()
      .isEmpty(),
    check('email', 'please, include a valid email').isEmail(),
    check(
      'password',
      'Invalid password. Please, enter a password with 8 or more characters including at least one special character and 2 Uppercase letters without empty spaces'
    )
      .isLength({ min: 8 })
      .matches(/^(?=.*[A-Z].*[A-Z])(?=.*[^A-Za-z0-9])(?=.*[0-9]).{8,16}$/),
    body('passwordConfirmation').custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Password confirmation does not match password');
      }
      return true;
    })
  ],
  usersController.createNewUser
);

// @route  PUT api/users
// @desc   Register route
// @access Private
router.delete('/', auth, usersController.deleteUser)
module.exports = router;
